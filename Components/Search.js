import React from 'react'
import {StyleSheet, View, Button, TextInput, FlatList, Text} from 'react-native'
import films from '../Helpers/filmsData.js'
import FilmItem from '../Components/FilmItem'

class Search extends React.Component {
    render(){
        return(
            <View style={styles.main_container}>
                <TextInput style= {styles.textinput} placeholder="Titre du film"/>
                <Button style={styles.button} title="Recherche" onPress={() => {console.log('OK')}}/>
                <FlatList 
                data={films}
                keyExtractor={(item) => item.id.toString()}
                 renderItem={({item}) => <FilmItem film= {item}/>}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        marginTop : 50,
        flex: 1
    },
    textinput:
    { marginLeft: 5,
        marginRight: 5,
        height: 50, 
        borderColor: '#000000',
        borderWidth: 1,
        paddingLeft: 5 
    },
    button: {
        height : 50
    }
})

export default Search;